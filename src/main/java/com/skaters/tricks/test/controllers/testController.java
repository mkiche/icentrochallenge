package com.skaters.tricks.test.controllers;

import com.skaters.tricks.test.entities.*;
import com.skaters.tricks.test.repos.SkateRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

//import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/gateway/api")
public class testController {


    private static final String CODE_SUCCESS = "00";
    private static final String CODE_ERROR = "01";

    @Autowired
    private SkateRepo skateRepo;


    //Post Request to add user
    @PostMapping(value = "/AddUser",
            consumes = {MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE },
            produces = {MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE} )
    public ApiResponses postAddUser(@Valid @RequestBody User user, Errors errors) throws  Exception {

        ApiResponses response = new ApiResponses();


        //orderValidator.cancelValidate(corder, errors);

        //check if errored add them accordingly
       // errors.reject("904","The transaction status does not allow cancelling.");

        if (errors.hasErrors()) {

            response.setMessage(errors.getAllErrors()
                    .stream()
                    .map(x -> x.getDefaultMessage())
                    .collect(Collectors.joining(",")));

            response.setStatusCode(CODE_ERROR );
        } else {

            //add user to DB
            int i =skateRepo.addUser(user.getFullName(), user.getUserName());

            if(i > 0) {
                response.setMessage("User Successfully added");
                response.setStatusCode(CODE_SUCCESS);
            } else {
                response.setMessage("Sorry, adding new user failed!");
                response.setStatusCode(CODE_ERROR);
            }
        }

        return response;
    } //end add user


    //Post Request to add tricks
    @PostMapping(value = "/AddTricks",
            consumes = {MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE },
            produces = {MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE} )
    public ApiResponses postAddTrick(@Valid @RequestBody Trick trick, Errors errors) throws  Exception {

        ApiResponses response = new ApiResponses();

        if (errors.hasErrors()) {

            response.setMessage(errors.getAllErrors()
                    .stream()
                    .map(x -> x.getDefaultMessage())
                    .collect(Collectors.joining(",")));

            response.setStatusCode(CODE_ERROR);
        } else {

            int i =skateRepo.addTrick (trick.getTrickTitle(), trick.getUserName(), trick.getTrickDescription());

            if(i > 0) {
                response.setMessage("User Successfully added");
                response.setStatusCode(CODE_SUCCESS);
            } else {
                response.setMessage("Sorry, adding new user failed!");
                response.setStatusCode(CODE_ERROR);
            }
        }

        return response;
    } //end add tricks


    //Post Request to get tricks
    @PostMapping(value = "/GetTricks",
            consumes = {MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE },
            produces = {MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE} )
    public ApiResponses postGetTricks(@Valid @RequestBody SavedTricks trick, Errors errors) throws  Exception {

        ApiResponses response = new ApiResponses();


        if (errors.hasErrors()) {

            response.setMessage(errors.getAllErrors()
                    .stream()
                    .map(x -> x.getDefaultMessage())
                    .collect(Collectors.joining(",")));

            response.setStatusCode(CODE_ERROR);
        } else {

            List<Trick> allTricks = skateRepo.getAllTricks();

            if(allTricks.isEmpty()){
                response.setMessage("No tricks saved as yet!");
                response.setStatusCode(CODE_ERROR);

            } else{
                response.setMessage(allTricks.get(0).toString());
                response.setStatusCode(CODE_SUCCESS);
            }
        }

        return response;
    } //end get tricks


    //Post Request to get tricks
    @PostMapping(value = "/FavouriteTrick",
            consumes = {MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE },
            produces = {MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE} )
    public ApiResponses postFavouriteTrick(@Valid @RequestBody Favourite fav, Errors errors) throws  Exception {

        ApiResponses response = new ApiResponses();

        if (errors.hasErrors()) {

            response.setMessage(errors.getAllErrors()
                    .stream()
                    .map(x -> x.getDefaultMessage())
                    .collect(Collectors.joining(",")));

            response.setStatusCode(CODE_ERROR);
        } else {

            int i =skateRepo.flagTrick  (fav.getTrickTitle(), fav.getUserName(), fav.isFavourite() );

            if(i > 0) {
                response.setMessage("Trick Successfully flagged as (un)favourite");
                response.setStatusCode(CODE_SUCCESS);
            } else {
                response.setMessage("Sorry, adding new user failed!");
                response.setStatusCode(CODE_ERROR);
            }


        }

        return response;
    } //end flag tricks





}
