package com.skaters.tricks.test.repos;

import com.skaters.tricks.test.entities.Trick;
import com.skaters.tricks.test.utilities.TrickMapper;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityNotFoundException;
import javax.sql.DataSource;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

@Repository
public class SkateRepo {


    private JdbcTemplate jdbcTemplate;


    @Bean(name = "myApiJdbcTemplate")
    public JdbcTemplate jdbcTemplate(@Qualifier("myApiDataSource") DataSource dataSource){
        this.jdbcTemplate = new JdbcTemplate(dataSource);
        return  jdbcTemplate;
    }

    public int addUser(String fullName, String userName) throws SQLException {

        try {
            String SQL_QUERY ="INSERT INTO public.users(fullname, username)VALUES (?, ?)";
            int updatedCols = jdbcTemplate.update(SQL_QUERY,
                    fullName , userName);
            return updatedCols;

        } catch (Exception ex) {
            ex.printStackTrace();
            return  0;
        }
    }

    public int addTrick(String title, String userName, String description) throws SQLException {

        try {
            String SQL_QUERY ="INSERT INTO public.tricks(username, title, description)VALUES (?, ?, ?)";
            int updatedCols = jdbcTemplate.update(SQL_QUERY,
                    userName , title, description);
            return updatedCols;

        } catch (Exception ex) {
            return  0;
        }
    }

    public int flagTrick(String title, String userName, boolean favourite) throws SQLException {

        try {
            String SQL_QUERY ="update public.tricks set favourite=? where username=? and title=?";
            int updatedCols = jdbcTemplate.update(SQL_QUERY,
                    favourite, userName , title);
            return updatedCols;

        } catch (Exception ex) {
            return  0;
        }
    }




    public List<Trick> getAllTricks() throws EntityNotFoundException {

        try {

            String SQL_QUERY = "SELECT username, title, description FROM public.tricks";

            List<Trick> collPoint = jdbcTemplate.query(SQL_QUERY,  new Object[] { } , new TrickMapper());
            return  collPoint;

        }catch (Exception ex){
            return  null;
        }

    }






}
