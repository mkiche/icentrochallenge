package com.skaters.tricks.test;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.boot.autoconfigure.jdbc.DataSourceTransactionManagerAutoConfiguration;
import org.springframework.context.ApplicationContext;

@SpringBootApplication(scanBasePackages={
        "com.skaters.tricks.test.controllers",
        "com.skaters.tricks.test.entities",
        "com.skaters.tricks.test.repos",
        "com.skaters.tricks.test.utilities"})
@EnableAutoConfiguration(exclude = {DataSourceAutoConfiguration.class, DataSourceTransactionManagerAutoConfiguration.class})
public class TestApplication implements CommandLineRunner {

	public static void main(String[] args) {
		//SpringApplication.run(TestApplication.class, args);
        ApplicationContext context = SpringApplication.run(TestApplication.class, args);
	}

    @Override
    public void run(String... args) throws Exception {

        //add hikari DB

        //create user table

        //create tricks table

    }



}

