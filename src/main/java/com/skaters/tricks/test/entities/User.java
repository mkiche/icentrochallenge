package com.skaters.tricks.test.entities;

import com.fasterxml.jackson.annotation.JsonProperty;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

@Entity
public class User {

    @JsonProperty("FullName")
    @NotEmpty(message = "Please provide your full name.")
    private String fullName;

    @Id
    @JsonProperty("Username")
    @NotEmpty(message = "Please provide a user name")
    private String userName;

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }
}
