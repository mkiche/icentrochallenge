package com.skaters.tricks.test.entities;

import com.fasterxml.jackson.annotation.JsonProperty;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.validation.constraints.NotEmpty;

@Entity
public class Favourite {

    @Id
    Integer Id;

    @JsonProperty("Favourite")
    @NotEmpty(message = "Mark your trick as a favourite or unfavorite.")
    private boolean favourite;

    @JsonProperty("TrickTitle")
    @NotEmpty(message = "Please provide a trick title")
    private String trickTitle;

    @JsonProperty("Username")
    @NotEmpty(message = "Please provide a user name")
    private String userName;


    public boolean isFavourite() {
        return favourite;
    }

    public void setFavourite(boolean favourite) {
        this.favourite = favourite;
    }

    public String getTrickTitle() {
        return trickTitle;
    }

    public void setTrickTitle(String trickTitle) {
        this.trickTitle = trickTitle;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    @Override
    public String toString() {
        return "Favourite{" +
                "favourite=" + favourite +
                ", trickTitle='" + trickTitle + '\'' +
                ", userName='" + userName + '\'' +
                '}';
    }

}
