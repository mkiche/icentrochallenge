package com.skaters.tricks.test.entities;

import com.fasterxml.jackson.annotation.JsonProperty;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.validation.constraints.NotEmpty;

@Entity
public class Trick {

    @Id
    Integer Id;

    @JsonProperty("Username")
    @NotEmpty(message = "Please provide a user name")
    private String userName;

    @JsonProperty("TrickTitle")
    @NotEmpty(message = "Please provide a trick title")
    private String trickTitle;

    @JsonProperty("TrickDescription")
    @NotEmpty(message = "Please provide a trick description")
    private String trickDescription;

    public String getUserName() {
        return userName;
    }

    @Override
    public String toString() {
        return "Trick{" +
                "userName='" + userName + '\'' +
                ", trickTitle='" + trickTitle + '\'' +
                ", trickDescription='" + trickDescription + '\'' +
                '}';
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getTrickTitle() {
        return trickTitle;
    }

    public void setTrickTitle(String trickTitle) {
        this.trickTitle = trickTitle;
    }

    public String getTrickDescription() {
        return trickDescription;
    }

    public void setTrickDescription(String trickDescription) {
        this.trickDescription = trickDescription;
    }




}
