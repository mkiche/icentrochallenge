package com.skaters.tricks.test.entities;

import com.fasterxml.jackson.annotation.JsonProperty;

import javax.persistence.Entity;
import javax.validation.constraints.NotEmpty;

//@Entity
public class SavedTricks {

    @JsonProperty("Count")
    @NotEmpty(message = "Please specify how many tricks you want to see")
    private Integer trickCount;

    public Integer getTrickCount() {
        return trickCount;
    }

    @Override
    public String toString() {
        return "SavedTricks{" +
                "trickCount='" + trickCount + '\'' +
                '}';
    }

    public void setTrickCount(Integer trickCount) {
        this.trickCount = trickCount;
    }



}
