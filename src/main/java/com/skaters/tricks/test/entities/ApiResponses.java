package com.skaters.tricks.test.entities;

public class ApiResponses {

    String StatusCode;
    String Message;

    @Override
    public String toString() {
        return "ApiResponses{" +
                "StatusCode=" + StatusCode +
                ", Message='" + Message + '}';
    }


    public String getStatusCode() {
        return StatusCode;
    }

    public void setStatusCode(String statusCode) {
        StatusCode = statusCode;
    }

    public String getMessage() {
        return Message;
    }

    public void setMessage(String message) {
        Message = message;
    }



}
