package com.skaters.tricks.test.utilities;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.autoconfigure.jdbc.DataSourceProperties;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.core.JdbcTemplate;

import javax.sql.DataSource;

@Configuration
public class MyDBConfiguration {

   @Autowired
   YAMLConfig myConfig;

    DataSourceProperties dsp = new DataSourceProperties();


   @Bean(name = "myApiProps")
    public DataSourceProperties dataSourceProperties(YAMLConfig myConfig) {
       this.myConfig = myConfig;
       dsp  = myConfig.getPostgreSQLDataSource();

       return dsp;
    }



   @Bean(name = "myApiDataSource")
    @ConfigurationProperties( "app.datasource")
    public DataSource dataSource( @Qualifier("myApiProps") DataSourceProperties properties){
        return properties.initializeDataSourceBuilder().build();
    }



   @Bean(name = "myApiJdbcTemp")
    public JdbcTemplate jdbcTemplate(@Qualifier("myApiDataSource") DataSource dataSource){

        return new JdbcTemplate(dataSource);
    }



}
