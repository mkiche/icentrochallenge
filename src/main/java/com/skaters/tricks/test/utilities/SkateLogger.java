package com.skaters.tricks.test.utilities;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class SkateLogger {

    public void logMessage(String fileName, String message)
    {
        String LogDir = "SKATE";
        LocalDateTime now = LocalDateTime.now();
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
        String formatDateTime = now.format(formatter);

        try{
            Files.createDirectories(Paths.get(LogDir));

            fileName = LogDir  + "/" + fileName;
            message = formatDateTime + "::" + message;

            File file = new File(fileName);
            file.createNewFile();
        } catch (Exception ex){
            ex.printStackTrace();
        }


        try(FileWriter fw = new FileWriter(fileName, true);
            BufferedWriter bw = new BufferedWriter(fw);
            PrintWriter out = new PrintWriter(bw))
        {
            out.println(message);
            out.println("");
        } catch (IOException e) {
            e.printStackTrace();
        }




    }

}
