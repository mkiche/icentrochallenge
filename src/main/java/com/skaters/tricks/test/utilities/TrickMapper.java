package com.skaters.tricks.test.utilities;

import com.skaters.tricks.test.entities.Trick;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class TrickMapper  implements RowMapper<Trick> {


    @Override
    public Trick mapRow(ResultSet rs, int i) throws SQLException {
        Trick trick = new Trick();
        trick.setTrickDescription(rs.getString("description"));
        trick.setTrickTitle(rs.getString("title"));
        trick.setUserName(rs.getString("username"));
        return trick;
    }
}
